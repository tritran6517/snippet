<?php
$pageid = "slide";
$scss = "dev/scss/3_project/_slide.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<section class="l-gl-box-wrap">
	<!-- Slide 1 -->
	<div class="c-gl-box" data-name="c-slide1">
		<div class="c-slide1">
            <div class="c-slide1__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="300" height="200"></div>
            <div class="c-slide1__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="300" height="200"></div>
            <div class="c-slide1__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="300" height="200"></div>
            <div class="c-slide1__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="300" height="200"></div>
            <div class="c-slide1__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="300" height="200"></div>
        </div>
	</div>
</section>
<section class="l-gl-box-wrap">
	<!-- Slide 2 -->
	<div class="c-gl-box" data-name="c-slide2">
		<div class="c-slide2">
            <div class="c-slide2__item" style="color: #fff; background: green">
                <p>Slide 1</p>
            </div>
            <div class="c-slide2__item" style="color: #fff; background: red">
                <p>Slide 2</p>
            </div>
            <div class="c-slide2__item" style="color: #fff; background: blue">
                <p>Slide 3</p>
            </div>
            <div class="c-slide2__item" style="color: #fff; background: black">
                <p>Slide 4</p>
            </div>
            <div class="c-slide2__item" style="color: #fff; background: purple">
                <p>Slide 5</p>
            </div>
        </div>
	</div>
</section>
<section class="l-gl-box-wrap">
	<!-- Slide 3 -->
	<div class="c-gl-box" data-name="c-slide3">
		<div class="c-slide3">
            <div class="c-slide3__inner">
                <div class="c-slide3__1">
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="700"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="700"></div>
                </div>
                <div class="c-slide3__2">
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="500"></div>
                    <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="500"></div>
                </div>
            </div>
            <div class="c-slide3__3">
                <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide1.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide2.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide4.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide6.jpg" alt="" width="500"></div>
                <div class="c-slide3__item" ><img src="/assets/img/common/slide5.jpg" alt="" width="500"></div>
            </div>
        </div>
	</div>
</section>
<section class="l-gl-box-wrap">
	<!-- Slide 4 -->
	<div class="c-gl-box" data-name="c-slide4">
		<section class="c-slide4">
            <div class="c-slide4__wrap">    
                <div class="c-slide4__item c-slide4__1" id="visualNumber_1"></div>
                <div class="c-slide4__item c-slide4__2" id="visualNumber_2"></div>
                <div class="c-slide4__item c-slide4__3" id="visualNumber_3"></div>
                <div class="c-slide4__item c-slide4__4" id="visualNumber_4"></div>
                <div class="c-slide4__item c-slide4__5" id="visualNumber_5"></div>
            </div>
            <ul class="c-slide4__nav">
                <li data-index="1"></li>
                <li data-index="2"></li>
                <li data-index="3"></li>
                <li data-index="4"></li>
                <li data-index="5"></li>
            </ul>
        </section>
    </div>
</section>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
