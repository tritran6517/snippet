<?php
$pageid = "animation-view";
$scss = "dev/scss/3_project/_animation-view.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<section class="l-gl-box-wrap">
	<!-- Animation view 1 -->
	<div class="c-gl-box" data-name="c-animation-view1">
		<section class="c-animation-view1">
			<a href="" class="c-animation-view1__link">
				<div class="c-animation-view2">
					<div class="c-animation-view2__content"></div>
				</div>
			</a>
		</section>
	</div>
</section>
<section class="l-gl-box-wrap">
	<!-- Animation view 2 -->
	<div class="c-gl-box" data-name="c-animation-view2">
		<section class="c-animation-view2">
			<div class="c-animation-view2__content"></div>
		</section>
	</div>
</section>
<section class="l-gl-box-wrap">
	<!-- Animation view 3 -->
	<div class="c-gl-box" data-name="c-animation-view3">
		<section class="c-animation-view3">
		</section>
	</div>
</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
