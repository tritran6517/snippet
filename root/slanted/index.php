<?php
$pageid = "slanted";
$scss = "dev/scss/3_project/_slanted.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<?php ////////////////////////////////////////////////
// p-slanted1
////////////////////////////////////////////////////// ?>
<div class="c-gl-box c-gl-box--wide" data-name="slanted-01">
	<div class="p-slanted1">
		<div class="p-slanted1-1">
			<div class="p-slanted1__img1">
				<img src="https://placehold.jp/1000x300.png" alt="">
			</div>
			<div class="p-slanted1__bg">
				<div class="p-slanted1__txt">
					<h2 class="p-slanted1__ttl">これはタイトルです</h2>
					<p class="p-slanted1__text">dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
				</div>
			</div>
		</div>
		<div class="p-slanted1-2">
			<div class="p-slanted1__img1">
				<img src="https://placehold.jp/1000x300.png" alt="">
			</div>
			<div class="p-slanted1__bg">
				<div class="p-slanted1__txt">
					<h2 class="p-slanted1__ttl">これはタイトルです</h2>
					<p class="p-slanted1__text">dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
				</div>
			</div>
		</div>
		<div class="p-slanted1-3">
			<div class="p-slanted1__img1">
				<img src="https://placehold.jp/1000x300.png" alt="">
			</div>
			<div class="p-slanted1__bg">
				<div class="p-slanted1__txt">
					<h2 class="p-slanted1__ttl">これはタイトルです</h2>
					<p class="p-slanted1__text">dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
				</div>
			</div>
		</div>
	</div>
</div>
<?php ////////////////////////////////////////////////
// p-slanted2
////////////////////////////////////////////////////// ?>
<div class="c-gl-box c-gl-box--wide" data-name="slanted-02">
	<div class="p-slanted2">
		<div class="p-slanted2__ttl">
			<h2 class="p-slanted2__main">SELECTED WORK</h2>
			<a href="#" class="p-slanted2__sub">See All</a>
		</div>
		<ul class="p-slanted2__list">
			<li>
				<a href="#" class="c-SlanCart">
					<div class="c-SlanCart__img">
						<div class="c-SlanCart__cont">
							<img src="https://placehold.jp/350x350.png" alt="">
						</div>
					</div>
					<div class="c-SlanCart__txt">
						<p>AMEX - LET'S GET BUSINES DONE</p>
					</div>
				</a>
			</li>
			<li>
				<a href="#" class="c-SlanCart">
					<div class="c-SlanCart__img">
						<div class="c-SlanCart__cont">
							<img src="https://placehold.jp/350x350.png" alt="">
						</div>
					</div>
					<div class="c-SlanCart__txt">
						<p>MCDONALD'S #FRITESPOTATOES</p>
					</div>
				</a>
			</li>
			<li>
				<a href="#" class="c-SlanCart">
					<div class="c-SlanCart__img">
						<div class="c-SlanCart__cont">
							<img src="https://placehold.jp/350x350.png" alt="">
						</div>
					</div>
					<div class="c-SlanCart__txt">
						<p>VAULT - REDUCE RISK. INCREASE <br>REWARDS</p>
					</div>
				</a>
			</li>
		</ul>
	</div>
</div>
<?php ////////////////////////////////////////////////
// p-slanted3
////////////////////////////////////////////////////// ?>
<div class="c-gl-box c-gl-box--wide" data-name="slanted-03">
	<div class="p-slanted3">
		<div class="p-slanted3__left">
			<div class="p-slanted3__img">
				<img src="http://flocssbem.test20008.com/sheet/roadmap/kadai/009.jpg" alt="">
			</div>
			<div class="p-slanted3__txt">
				<h2 class="p-slanted3__main">HISTORY</h2>
				<p class="p-slanted3__sub">dummy dummy</p>
			</div>
		</div>
		<div class="p-slanted3__right">
			<div class="p-slanted3__img">
				<img src="http://flocssbem.test20008.com/sheet/roadmap/kadai/010.jpg" alt="">
			</div>
			<div class="p-slanted3__txt">
				<h2 class="p-slanted3__main">HISTORY</h2>
				<p class="p-slanted3__sub">dummy dummy</p>
			</div>
		</div>
	</div>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>