<?php 
$pageid = "title";
$scss = "dev/scss/3_project/_title.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>


<?php ////////////////////////////////////////////////
// c-title1
////////////////////////////////////////////////////// ?>

<div class="c-gl-box c-gl-box--wide" data-name="c-title1">

	<h1 class="c-title1">見出し</h1>

	<h1 class="c-title1">見出し<br>複数行</h1>

	<p class="c-title1">
		<button>ボタン</button>
	</p>

</div>


<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>