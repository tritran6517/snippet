<?php
$pageid = "navigation";
$scss = "dev/scss/3_project/_navigation.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<article class="p-movie">

	<ul>
		<li><a href="/navigation/navigation001.php">001 - Vertical fixed navigation</a></li>
		<li><a href="/navigation/navigation002.php">002 - Scroll fix navigation</a></li>
		<li><a href="/navigation/navigation003.php">003 - Scroll fix navigation</a></li>
		<li><a href="/navigation/navigation004.php">004 - Scroll fix navigation</a></li>
		<li><a href="/navigation/navigation005.php">005 - Mega menu</a></li>
		<li><a href="/navigation/navigation002.php">006 - Mega menu</a></li>
		<li><a href="/navigation/navigation003.php">007 - Mega menu</a></li>
		<li><a href="/navigation/navigation004.php">008 - Step bar</a></li>
		<li><a href="/navigation/navigation004.php">009 - Step bar</a></li>
		<li><a href="/navigation/navigation005.php">010 - Step bar</a></li>
	</ul>

</article>



<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>