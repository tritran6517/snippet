<?php
$pageid = "icon";
$scss = "dev/scss/3_project/_icon.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-icon1
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon1">

	<div class="c-icon1">
		<span class="c-icon1__line"></span>
		<span class="c-icon1__line"></span>
		<span class="c-icon1__line"></span>
	</div>

</div>

<script>
	$(function(){
		$('.c-icon1').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>


<?php ////////////////////////////////////////////////
// c-icon2
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon2">

	<div class="c-icon2">
		<span class="c-icon2__line"></span>
		<span class="c-icon2__line"></span>
		<span class="c-icon2__line"></span>
	</div>

</div>

<script>
	$(function(){
		$('.c-icon2').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>

<?php ////////////////////////////////////////////////
// c-icon3
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon3">

	<div class="c-icon3">
		<span class="c-icon3__line"></span>
		<span class="c-icon3__line"></span>
		<span class="c-icon3__line"></span>
	</div>

</div>

<script>
	$(function(){
		$('.c-icon3').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>


<?php ////////////////////////////////////////////////
// c-icon4
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon4">

	<div class="c-icon4">
		<span class="c-icon4__line"></span>
		<span class="c-icon4__line"></span>
		<span class="c-icon4__line"></span>
	</div>

</div>

<script>
	$(function(){
		$('.c-icon4').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>

</section>





<section class="l-gl-box-wrap">


<?php ////////////////////////////////////////////////
// c-icon11
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon11">

	<div class="c-icon11"></div>

</div>

<script>
	$(function(){
		$('.c-icon11').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>

<?php ////////////////////////////////////////////////
// c-icon12
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon12">

	<div class="c-icon12"></div>

</div>

<script>
	$(function(){
		$('.c-icon12').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>

<?php ////////////////////////////////////////////////
// c-icon13
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-icon13">

	<div class="c-icon13"></div>

</div>

<script>
	$(function(){
		$('.c-icon13').on('click',function(){
			$(this).toggleClass('is-active');
		});
	});
</script>


</section>


<?php ////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////// ?>
<script>
	$(function(){
    setInterval(function(){
    	for(i=1;i<=100;i++){
			$('.c-icon'+i).toggleClass('is-active');
		}
    },1500);
	});
</script>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
