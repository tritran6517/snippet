<?php
$pageid = "form";
$scss = "dev/scss/3_project/_form.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-input1
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input1">

	<div class="c-input1">
		<input type="radio" name="aaa1" checked>
		<label>Radio</label>
	</div>

	<div class="c-input1">
		<input type="radio" name="aaa1">
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input1">
		<input type="checkbox" checked>
		<label>Checkbox</label>
	</div>

	<div class="c-input1">
		<input type="checkbox">
		<label>Checkbox</label>
	</div>

	<br><br>
	size
	<br><br>

	<div class="c-input1 c-input1--fill">
		<input type="checkbox" checked>
		<label>Fill</label>
	</div>

	<div class="c-input1 c-input1--thick">
		<input type="checkbox" checked>
		<label>Thick</label>
	</div>

</div>


<?php ////////////////////////////////////////////////
// c-input2
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input2">

	<div class="c-input2">
		<input type="radio" name="aaa2" checked>
		<label>Radio</label>
	</div>

	<div class="c-input2">
		<input type="radio" name="aaa2">
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input2">
		<input type="checkbox" checked>
		<label>Checkbox</label>
	</div>

	<div class="c-input2">
		<input type="checkbox">
		<label>Checkbox</label>
	</div>

	<br><br>
	size
	<br><br>

	<div class="c-input2 c-input2--fill">
		<input type="checkbox" checked>
		<label>Fill</label>
	</div>

	<div class="c-input2 c-input2--thick">
		<input type="checkbox" checked>
		<label>Thick</label>
	</div>

</div>

</section>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-input3
////////////////////////////////////////////////////// ?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

<div class="c-gl-box" data-name="c-input3">

	fontawesomeを使ってアイコンを表示

	<br><br>

	<div class="c-input3">
		<input type="Radio" name="aaa3" checked>
		<i class="fas fa-check"></i>
		<label>Radio</label>
	</div>

	<div class="c-input3">
		<input type="radio" name="aaa3">
		<i class="fas fa-check"></i>
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input3">
		<input type="checkbox" checked>
		<i class="fas fa-check"></i>
		<label>Checkbox</label>
	</div>

	<div class="c-input3">
		<input type="checkbox">
		<i class="fas fa-check"></i>
		<label>Checkbox</label>
	</div>

</div>


<?php ////////////////////////////////////////////////
// c-input4
////////////////////////////////////////////////////// ?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">

<div class="c-gl-box" data-name="c-input4">

	fontawesomeを使ってアイコンを表示

	<br><br>

	<div class="c-input4">
		<input type="Radio" name="aaa4" checked>
		<i class="fas fa-check"></i>
		<label>Radio</label>
	</div>

	<div class="c-input4">
		<input type="radio" name="aaa4">
		<i class="fas fa-check"></i>
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input4">
		<input type="checkbox" checked>
		<i class="fas fa-check"></i>
		<label>Checkbox</label>
	</div>

	<div class="c-input4">
		<input type="checkbox">
		<i class="fas fa-check"></i>
		<label>Checkbox</label>
	</div>

</div>

</section>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-input5
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input5">

	SVGの「コード」を使ってアイコン表示

	<br><br>

	<div class="c-input5">
		<input type="radio" name="aaa5" checked>
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Radio</label>
	</div>

	<div class="c-input5">
		<input type="radio" name="aaa5">
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input5">
		<input type="checkbox" checked>
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Checkbox</label>
	</div>

	<div class="c-input5">
		<input type="checkbox">
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Checkbox</label>
	</div>

</div>

<?php ////////////////////////////////////////////////
// c-input6
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input6">

	SVGの「コード」を使ってアイコン表示

	<br><br>

	<div class="c-input6">
		<input type="radio" name="aaa6" checked>
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Radio</label>
	</div>

	<div class="c-input6">
		<input type="radio" name="aaa6">
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input6">
		<input type="checkbox" checked>
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Checkbox</label>
	</div>

	<div class="c-input6">
		<input type="checkbox">
		<svg viewBox="0 0 20 20">
			<path d="M7.629,14.566c0.125,0.125,0.291,0.188,0.456,0.188c0.164,0,0.329-0.062,0.456-0.188l8.219-8.221c0.252-0.252,0.252-0.659,0-0.911c-0.252-0.252-0.659-0.252-0.911,0l-7.764,7.763L4.152,9.267c-0.252-0.251-0.66-0.251-0.911,0c-0.252,0.252-0.252,0.66,0,0.911L7.629,14.566z"></path>
		</svg>
		<label>Checkbox</label>
	</div>

</div>

</section>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-input7
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input7">

	SVGやPNGの「ファイル」を使ってアイコン表示

	<br><br>

	<div class="c-input7">
		<input type="radio" name="aaa7" checked>
		<img src="image/002.svg">
		<label>Radio</label>
	</div>

	<div class="c-input7">
		<input type="radio" name="aaa7">
		<img src="image/002.svg">
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input7">
		<input type="checkbox" checked>
		<img src="image/002.svg">
		<label>Checkbox</label>
	</div>

	<div class="c-input7">
		<input type="checkbox">
		<img src="image/002.svg">
		<label>Checkbox</label>
	</div>

	<div class="c-input7">
		<input type="checkbox" checked>
		<img src="image/001.png">
		<label>Checkbox</label>
	</div>

</div>

<?php ////////////////////////////////////////////////
// c-input8
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-input8">

	SVGやPNGの「ファイル」を使ってアイコン表示

	<br><br>

	<div class="c-input8">
		<input type="radio" name="aaa8" checked>
		<img class="svg" src="image/002.svg">
		<label>Radio</label>
	</div>

	<div class="c-input8">
		<input type="radio" name="aaa8">
		<img class="svg" src="image/002.svg">
		<label>Radio</label>
	</div>

	<br><br>

	<div class="c-input8">
		<input type="checkbox" checked>
		<img class="svg" src="image/002.svg">
		<label>Checkbox</label>
	</div>

	<div class="c-input8">
		<input type="checkbox">
		<img class="svg" src="image/002.svg">
		<label>Checkbox</label>
	</div>

	<div class="c-input8">
		<input type="checkbox" checked>
		<img src="image/001.png">
		<label>Checkbox</label>
	</div>

</div>

</section>



<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-select1
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-select1">

	背景画像でアイコンを表示した
	<br><br>

	<div class="c-select1">
		<select>
			<option value="">Select</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select>
	</div>

	<br>

	<div class="c-select1 c-select1--color1">
		<select>
			<option value="">Select</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select>
	</div>

</div>

<?php ////////////////////////////////////////////////
// c-select2
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-select2">

	:before、:afterでアイコンを表示した
	<br><br>

	<div class="c-select2">
		<select>
			<option value="">Select</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select>
	</div>

	<br>

	<div class="c-select2 c-select2--color1">
		<select>
			<option value="">Select</option>
			<option value="">1</option>
			<option value="">2</option>
			<option value="">3</option>
		</select>
	</div>

</div>

</section>














<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>