<?php 
$pageid = "movie001";
$scss = "dev/scss/3_project/_movie.scss";
$myPath = __FILE__;

?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<section class="l-gl-box-wrap">
    <!-- Responsive 1 -->
	<div class="c-gl-box" data-name="c-movie1">
        <div class="c-movie1">
            <p>Responsive Youtube: Conditional</p><br/>
            <iframe width="853" height="480" src="https://www.youtube.com/embed/a2GujJZfXpg" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</section>
<section class="l-gl-box-wrap">
    <!-- Responsive 2 -->
	<div class="c-gl-box" data-name="c-movie2">
        <div class="c-movie2">
            <br/><br/><br/><br/><br/><br/><br/><br/>
            <p>Responsive Youtube: No Conditional</p>
            <iframe width="853" height="480" src="https://www.youtube.com/embed/a2GujJZfXpg" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>