<?php
$pageid = "movie";
$scss = "dev/scss/3_project/_movie.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<article class="p-movie">

	<ul>
		<li><a href="/movie/movie001.php">001 - Youtube Responsive</a></li>
		<li><a href="/movie/movie002.php">002 - Background movie - Youtube</a></li>
		<li><a href="/movie/movie003.php">003 - Background movie - HTML5 Video</a></li>
		<li><a href="/movie/movie004.php">004 - </a></li>
		<li><a href="/movie/movie005.php">005 - </a></li>
		<li><a href="/movie/movie006.php">006 - </a></li>
		<li><a href="/movie/movie007.php">007 - </a></li>
		<li><a href="/movie/movie008.php">008 - </a></li>
		<li><a href="/movie/movie009.php">009 - </a></li>
		<li><a href="/movie/movie010.php">010 - </a></li>
	</ul>

</article>



<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>