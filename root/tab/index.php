<?php
$pageid = "tab";
$scss = "dev/scss/3_project/_tab.scss";
$myPath = __FILE__;
?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<section class="l-gl-box-wrap">

<?php ////////////////////////////////////////////////
// c-tab1
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-tab1 / index()版">

	<nav class="c-tab-nav1">
		<div class="c-tab-nav1__list is-active">tab1</div>
		<div class="c-tab-nav1__list">tab2</div>
		<div class="c-tab-nav1__list">tab3</div>
	</nav>
	<div class="c-tab-content1">
		<div class="c-tab-content1__block is-active">Content 1</div>
		<div class="c-tab-content1__block">Content 2</div>
		<div class="c-tab-content1__block">Content 3</div>
	</div>

</div>

<script>
$(function(){

	var $navElm = $('.c-tab-nav1__list');
	var activeClass = "is-active";

	$navElm.on('click', function() {

		var index = $navElm.index(this);
		$navElm.removeClass(activeClass);
		$(this).addClass(activeClass);

		$('.c-tab-content1__block')
			.removeClass(activeClass)
			.eq(index)
			.addClass(activeClass);

	});

});
</script>


<?php ////////////////////////////////////////////////
// c-tab2
////////////////////////////////////////////////////// ?>
<div class="c-gl-box" data-name="c-tab2 / data版 （ナビがどこに何個あってもいい版）">

	<div data-tabindex="3" class="c-tab-nav2">tab3</div>
	<div data-tabindex="2" class="c-tab-nav2">tab2</div>
	<div data-tabindex="1" class="c-tab-nav2 is-active">tab1</div>
	<div data-tabindex="3" class="c-tab-nav2">tab3</div>

	<div class="c-tab-content2">
		<div data-tabindex="1" class="c-tab-content2__block is-active">Content 1</div>
		<div data-tabindex="2" class="c-tab-content2__block">Content 2</div>
		<div data-tabindex="3" class="c-tab-content2__block">Content 3</div>
	</div>

	<div data-tabindex="2" class="c-tab-nav2">tab2</div>

</div>

<script>
$(function(){

	var $navElm = $('.c-tab-nav2');
	var activeClass = "is-active";

	$navElm.on('click', function() {

		var tabIndex = $(this).data('tabindex');

		$navElm.removeClass(activeClass);
		$(this).addClass(activeClass);

		$('.c-tab-content2__block').removeClass(activeClass)
		$(".c-tab-content2__block[data-tabindex='"+tabIndex+"']").addClass(activeClass);

	});

});
</script>


</section>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
