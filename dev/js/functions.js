//=========================================
// global component クラス名表示
//=========================================
$(function () {
	$(".c-gl-box").each(function (index) {
		if ($(this).data("name") !== undefined) {
			$(this).prepend(
				'<div class="c-gl-box__no">' + $(this).data("name") + "</div>"
			);
		}
	});
});

//=========================================
// global component リンク
//=========================================
$(function () {
	$(".c-gl-box").each(function (index) {
		if ($(this).data("href") !== undefined) {
			$(this).on('click', function () {
				location.href = $(this).data("href");
			});
		}
	});
});
//////////////////////////////////////////////////////////
// Recruit Search
//////////////////////////////////////////////////////////
$(document).ready(function () {
	$(".c-table1 tr th:first-child").click(function () {
		$(this).toggleClass("open");
		$(".c-table1 tr td").toggleClass("active");
	});

	$(".c-table1__btn").click(function (e) {
		e.preventDefault();
		$(".c-input input:checkbox").removeAttr("checked");
	});
});
//=========================================
// global component 滑り台
//=========================================
$(function () {
	// Slide 1 - ゆっくりと流れる画像
	$('.c-slide1').slick({
		speed: 5000,
		autoplay: true,
		autoplaySpeed: 0,
		centerMode: true,
		cssEase: 'linear',
		slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		infinite: true,
		initialSlide: 1,
		arrows: false,
		dots: false
	});

	$('.c-slide2').slick({
		centerMode: true,
		arrows: true,
		dots: true,
		centerPadding: '150px',
	})
	$('.c-slide3__1').slick({
		fade: false,
		arrows: true,
		asNavFor: '.c-slide3__2,.c-slide3__3'
	})
	$('.c-slide3__2').slick({
		asNavFor: '.c-slide3__1,.c-slide3__3',
		arrows: false,
		centerMode: true,
		dots: false,
		focusOnSelect: true,
		slidesToShow: 1,
		fade: true
	})
	$('.c-slide3__3').slick({
		fade: false,
		arrows: true,
		asNavFor: '.c-slide3__1,.c-slide3__2',
		autoplay: true,
		speed: 500,
		cssEase: 'linear',
		slidesToShow: 5,
		slidesToScroll: 1
	})
});

//=========================================
// global component アニメーション
//=========================================
$(function () {
	setTimeout(() => {
		$(".c-animation-view1").addClass("is-scroll");
		$(".c-animation-view2").addClass("is-scroll");
		$(".c-animation-view3").addClass("is-active");
	}, 1000);
});
if ($("body").hasClass("page-slide")) {
	$(function () {
		var o = $(".c-slide4__item"),
			s = $(".c-slide4__nav li"),
			e = 0,
			t = 6000,
			a = 0,
			r = 0,
			l = 0,
			d = s.length - 1;

		function setItem() {
			o.eq(e).addClass("is-current");
			s.eq(e).addClass("is-current");
			a = setInterval(animationLoop, t);
		}

		function animationLoop() {
			var clickHideNumber = $(".c-slide4__item.is-current").attr("id").replace("visualNumber_", "");
			l = 1;
			0 === r && (e === d ? e = 0 : e++);
			s.removeClass("is-current");
			s.eq(e).addClass("is-current");
			o.animate({
				opacity: 0
			}, {
				duration: 1000,
				queue: false
			});
			o.eq(e).animate({
				opacity: 1
			}, 1000);
			o.removeClass("is-current");
			o.eq(e).addClass("is-current");
			1 === r ?
				o.eq(clickHideNumber - 1).addClass("is-hide") :
				o.eq(e - 1).addClass("is-hide"),
				setTimeout(function () {
					o.removeClass("is-hide");
					$('.c-slide4__item:not(".is-current")').addClass("is-def");
				}, 900),
				setTimeout(function () {
					o.removeClass("is-def"),
						1 === r && (r = 0),
						l = 0
				}, 900)
		}
		$(".c-slide4__nav li").on("click", function () {
			var clickHideNumber = $(".c-slide4__item.is-current").attr("id").replace("visualNumber_", "");
			!$(this).hasClass("is-current") && 1 !== l && (r = 1, e = $(this).data("index") - 1);
			clickHideNumber;
			clearInterval(a);
			animationLoop();
			a = setInterval(animationLoop, t);
		});
		setItem();
	});
}
